#pragma once
#ifndef SCL_UNARY_OPERATOR_H
#define SCL_UNARY_OPERATOR_H

#include <ModelKit/Featuring/Access/HolderInternal.h>
#include <ModelKit/Utility/IsMethodExists.h>
#include <ModelKit/Utility/IsOperatorExists.h>
#include <ModelKit/Utility/SingleArgument.h>
#include "ResultSwitch.h"

namespace ScL { namespace Detail
{
    namespace Operator
    {
        namespace Unary
        {
            /* Cases for Holder method existing */
            struct HolderHasOperatorCase {};
            struct HolderHasNoOperatorCase {};
        }
    }
}}

#define SCL_IS_UNARY_OPERATOR_EXISTS_TEST_TRAIT( Invokable ) \
    template < typename _Kind, typename _Refer, typename ... _Arguments > \
    struct Is ## Invokable ## OperatorExistsTestHelper; \
     \
    template < typename _Refer, typename ... _Arguments > \
    using Is ## Invokable ## OperatorExistsTest = Is ## Invokable ## OperatorExistsTestHelper< ::ScL::Detail::Operator::InstanceSwitchCase< _Refer >, _Refer, _Arguments ... >; \
     \
    template < typename _Refer, typename ... _Arguments > \
    /*inline*/ constexpr bool is_ ## Invokable ## _operator_exists_test = Is ## Invokable ## OperatorExistsTest< _Refer, _Arguments ... >::value; \
     \
    template < typename _Refer, typename ... _Arguments > \
    inline constexpr bool is ## Invokable ## OperatorExistsTest () { return Is ## Invokable ## OperatorExistsTest< _Refer, _Arguments ... >::value; } \
     \
    template < typename _Refer, typename ... _Arguments > \
    struct Is ## Invokable ## OperatorExistsTestHelper< ::ScL::Detail::Operator::NoneInstanceCase, _Refer, _Arguments ... > \
    { \
        static_assert( ::std::is_reference< _Refer >::value, "The template parameter _Refer must to be a reference type." ); \
     \
        static const bool value = ::ScL::Detail::Operator::Unary::is_ ## Invokable ## _operator_exists< _Refer, _Arguments ... >; \
    }; \
     \
    template < typename _Refer, typename ... _Arguments > \
    struct Is ## Invokable ## OperatorExistsTestHelper< ::ScL::Detail::Operator::UnaryInstanceCase, _Refer, _Arguments ...  > \
    { \
        static_assert( ::std::is_reference< _Refer >::value, "The template parameter _Refer must to be a reference type." ); \
        using InstanceRefer = _Refer; \
        using Instance = ::std::decay_t< InstanceRefer >; \
        using Holder = typename Instance::Holder; \
        using HolderRefer = ::ScL::SimilarRefer< Holder, InstanceRefer >; \
        using Value = typename Instance::Value; \
        using ValueRefer = ::ScL::SimilarRefer< Value, InstanceRefer >; \
     \
        static const bool value = ::ScL::Detail::Operator::Unary::is_operator ## Invokable ## _method_exists< Holder, void(HolderRefer, _Arguments ...) > \
            || ::ScL::Detail::Operator::Unary::is_ ## Invokable ## _operator_exists< ValueRefer, _Arguments ...  >; \
    }; \
     \

#define SCL_COMMON_UNARY_OPERATOR_IMPLEMENTAION( Invokable ) \
    namespace ScL { namespace Detail \
    { \
        namespace Operator \
        { \
            namespace Unary \
            { \
                template < typename > \
                struct Invokable ## Switch; \
         \
                template <> \
                struct Invokable ## Switch< ::ScL::Detail::Operator::Unary::HolderHasOperatorCase > \
                { \
                    template < typename _Instance, typename ... _Arguments > \
                    static decltype(auto) invoke ( _Instance && instance, _Arguments && ... arguments ) \
                    { \
                        using InstanceRefer = _Instance &&; \
                        using Holder = typename ::std::decay_t< InstanceRefer >::Holder; \
                        Holder::operator ## Invokable( ::ScL::Detail::instanceHolder( ::std::forward< InstanceRefer >( instance ) ), ::std::forward< _Arguments >( arguments ) ... ); \
                        return ::std::forward< InstanceRefer >( instance ); \
                    } \
                }; \
         \
                template <> \
                struct Invokable ## Switch< ::ScL::Detail::Operator::Unary::HolderHasNoOperatorCase > \
                { \
                    template < typename _Instance, typename ... _Arguments > \
                    static decltype(auto) invoke ( _Instance && instance, _Arguments && ... arguments ) \
                    { \
                        using InstanceRefer = _Instance &&; \
                        using ValueRefer = ::ScL::SimilarRefer< typename ::std::decay_t< InstanceRefer >::Value, InstanceRefer >; \
                        using Returned = ::std::result_of_t< Invokable( ValueRefer, _Arguments && ... ) >; \
                        return ::ScL::Detail::Operator::ResultSwitch< ::ScL::Detail::Operator::UnaryInstanceCase, ::ScL::Detail::Operator::ResultSwitchCase< Returned, ValueRefer > > \
                            ::invoke( ::ScL::Detail::Operator::Unary::Invokable(), ::std::forward< InstanceRefer >( instance ), ::std::forward< _Arguments >( arguments ) ... ); \
                    } \
                }; \
            } \
        } \
    }} \
     \
    namespace ScL { namespace Detail \
    { \
        namespace Operator \
        { \
            namespace Unary \
            { \
                template < typename > \
                struct Invokable ## InstanceSwitch; \
         \
                template <> \
                struct Invokable ## InstanceSwitch < ::ScL::Detail::Operator::UnaryInstanceCase > \
                { \
                    template < typename _Refer, typename ... _Arguments > \
                    static constexpr decltype(auto) invoke ( _Refer && value, _Arguments && ... arguments ) \
                    { \
                        using InstanceRefer = _Refer &&; \
                        using Instance = ::std::decay_t< InstanceRefer >; \
                        using Holder = typename Instance::Holder; \
                        using HolderRefer = ::ScL::SimilarRefer< Holder, InstanceRefer >; \
         \
                        constexpr bool holder_has_method_for_operator = ::ScL::Detail::Operator::Unary::is_operator ## Invokable ## _method_exists< Holder, void( HolderRefer, _Arguments && ... ) >; \
                        using OperatorSwitchCase = ::std::conditional_t< holder_has_method_for_operator, ::ScL::Detail::Operator::Unary::HolderHasOperatorCase, ::ScL::Detail::Operator::Unary::HolderHasNoOperatorCase >; \
                        return ::ScL::Detail::Operator::Unary::Invokable ## Switch< OperatorSwitchCase >::invoke( ::std::forward< InstanceRefer >( value ), ::std::forward< _Arguments >( arguments ) ... ); \
                    } \
                }; \
            } \
        } \
    }} \
     \
    namespace ScL { namespace Detail \
    { \
        namespace Operator \
        { \
           namespace Unary \
           { \
               template < typename _Refer, typename ... _Arguments > \
               struct Invokable ## Helper \
               { \
                   static_assert( ::std::is_reference< _Refer >::value, "The template parameter _Refer must to be a reference type." ); \
                   using Refer = _Refer; \
         \
                    static constexpr decltype(auto) invoke( Refer value, _Arguments && ... arguments ) \
                    { \
                        return ::ScL::Detail::Operator::Unary::Invokable ## InstanceSwitch< ::ScL::Detail::Operator::InstanceSwitchCase< Refer > > \
                            ::invoke( ::std::forward< Refer >( value ), ::std::forward< _Arguments && >( arguments ) ... ); \
                    } \
                }; \
            } \
        } \
    }} \

#define SCL_POSTFIX_UNARY_OPERATOR_IMPLEMENTAION( symbol, Invokable ) \
    namespace ScL { namespace Detail \
    { \
        namespace Operator \
        { \
            namespace Unary \
            { \
                SCL_IS_POSTFIX_UNARY_OPERATOR_EXISTS_TRAIT( SCL_SINGLE_ARG( symbol ), Invokable ) \
                SCL_IS_METHOD_EXISTS_TRAIT( operator ## Invokable ) \
                SCL_IS_UNARY_OPERATOR_EXISTS_TEST_TRAIT( Invokable ) \
            } \
        } \
    }} \
     \
    namespace ScL { namespace Detail \
    { \
        namespace Operator \
        { \
            namespace Unary \
            { \
                struct Invokable \
                { \
                    template < typename _Value > \
                    decltype(auto) operator () ( _Value && value ) \
                    { \
                        return ::std::forward< _Value && >( value ) symbol; \
                    } \
                }; \
            } \
        } \
    }} \
     \
    SCL_COMMON_UNARY_OPERATOR_IMPLEMENTAION( Invokable ) \

#define SCL_PREFIX_UNARY_OPERATOR_IMPLEMENTAION( symbol, Invokable ) \
    namespace ScL { namespace Detail \
    { \
        namespace Operator \
        { \
            namespace Unary \
            { \
                SCL_IS_PREFIX_UNARY_OPERATOR_EXISTS_TRAIT( SCL_SINGLE_ARG( symbol ), Invokable ) \
                SCL_IS_METHOD_EXISTS_TRAIT( operator ## Invokable ) \
                SCL_IS_UNARY_OPERATOR_EXISTS_TEST_TRAIT( Invokable ) \
            } \
        } \
    }} \
     \
    namespace ScL { namespace Detail \
    { \
        namespace Operator \
        { \
            namespace Unary \
            { \
                struct Invokable \
                { \
                    template < typename _Value > \
                    decltype(auto) operator () ( _Value && value ) \
                    { \
                        return symbol ::std::forward< _Value >( value ); \
                    } \
                }; \
            } \
        } \
    }} \
     \
    SCL_COMMON_UNARY_OPERATOR_IMPLEMENTAION( Invokable ) \


#define SCL_POSTFIX_UNARY_OPERATOR_WITH_ARGUMENT_IMPLEMENTAION( symbol, Invokable ) \
    namespace ScL { namespace Detail \
    { \
        namespace Operator \
        { \
            namespace Unary \
            { \
                SCL_IS_POSTFIX_UNARY_OPERATOR_WITH_ARGUMENT_EXISTS_TRAIT( SCL_SINGLE_ARG( symbol ), Invokable ) \
                SCL_IS_METHOD_EXISTS_TRAIT( operator ## Invokable ) \
                SCL_IS_UNARY_OPERATOR_EXISTS_TEST_TRAIT( Invokable ) \
            } \
        } \
    }} \
     \
    namespace ScL { namespace Detail \
    { \
        namespace Operator \
        { \
            namespace Unary \
            { \
                struct Invokable \
                { \
                    template < typename _Value, typename _Argument > \
                    decltype(auto) operator () ( _Value && value, _Argument && argument ) \
                    { \
                        return ::std::forward< _Value >( value ).operator symbol ( ::std::forward< _Argument >( argument ) ); \
                    } \
                }; \
            } \
        } \
    }} \
     \
    SCL_COMMON_UNARY_OPERATOR_IMPLEMENTAION( Invokable ) \

#define SCL_POSTFIX_UNARY_OPERATOR_WITH_ARGUMENTS_IMPLEMENTAION( symbol, Invokable ) \
    namespace ScL { namespace Detail \
    { \
        namespace Operator \
        { \
            namespace Unary \
            { \
                SCL_IS_POSTFIX_UNARY_OPERATOR_WITH_ARGUMENTS_EXISTS_TRAIT( SCL_SINGLE_ARG( symbol ), Invokable ) \
                SCL_IS_METHOD_EXISTS_TRAIT( operator ## Invokable ) \
                SCL_IS_UNARY_OPERATOR_EXISTS_TEST_TRAIT( Invokable ) \
            } \
        } \
    }} \
     \
    namespace ScL { namespace Detail \
    { \
        namespace Operator \
        { \
            namespace Unary \
            { \
                struct Invokable \
                { \
                    template < typename _Value, typename ... _Arguments > \
                    decltype(auto) operator () ( _Value && value, _Arguments && ... arguments ) \
                    { \
                        return ::std::forward< _Value >( value ).operator symbol ( ::std::forward< _Arguments >( arguments ) ... ); \
                    } \
                }; \
            } \
        } \
    }} \
     \
    SCL_COMMON_UNARY_OPERATOR_IMPLEMENTAION( Invokable ) \

#endif
